package com.chainsword.decaydisplay;

import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.transform.Rotate;

// **********************************************************************************
// Title: DecayDisplay
// Author: Jarom Wright
// Course Section: CMIS201-ONL3 (Seidel) Spring 2023
// File: RadiatedParticle.java
// Description: something.
// **********************************************************************************
public class RadiationParticle {
    final int type;
    Node particlePane;
    @SuppressWarnings("RedundantLabeledSwitchRuleCodeBlock")
    public RadiationParticle(int type, boolean emitting) {
        switch (type) {
            case 1 -> {
                //Proton
                particlePane = NucleusGeneration.makeProton(new Circle());
            }
            case 2 -> {
                //Neutron
                particlePane = NucleusGeneration.makeNeutron(new Circle());
            }
            case 3,5 -> {
                //Beta/Electron
                particlePane = NucleusGeneration.makeElectron(new Circle());
            }
            case 4 -> {
                //Alpha
                particlePane = new Pane();

                Circle proton1 = NucleusGeneration.makeProton(new Circle());
                ((Pane) particlePane).getChildren().add(proton1);
                proton1.centerXProperty().bind(NucleusGeneration.hadronRadius);
                proton1.centerYProperty().bind(NucleusGeneration.hadronRadius);

                Circle neutron1 = NucleusGeneration.makeNeutron(new Circle());
                ((Pane) particlePane).getChildren().add(neutron1);
                neutron1.centerXProperty().bind(NucleusGeneration.hadronRadius.multiply(-1));
                neutron1.centerYProperty().bind(NucleusGeneration.hadronRadius);

                Circle proton2 = NucleusGeneration.makeProton(new Circle());
                ((Pane) particlePane).getChildren().add(proton2);
                proton2.centerXProperty().bind(NucleusGeneration.hadronRadius.multiply(-1));
                proton2.centerYProperty().bind(NucleusGeneration.hadronRadius.multiply(-1));
                
                Circle neutron2 = NucleusGeneration.makeNeutron(new Circle());
                ((Pane) particlePane).getChildren().add(neutron2);
                neutron2.centerXProperty().bind(NucleusGeneration.hadronRadius);
                neutron2.centerYProperty().bind(NucleusGeneration.hadronRadius.multiply(-1));
                Rotate rotate = new Rotate(Math.random()*360);
                particlePane.getTransforms().add(rotate);
                particlePane.setRotate(Math.random());
            }
            case 6 -> {
                //Gamma
                //Pane lightningBolt1 = new Pane();
                Pane lightningBolt1 = new Pane();
                {
                    lightningBolt1.prefHeightProperty().bind(NucleusGeneration.hadronRadius.multiply(2));
                    lightningBolt1.prefWidthProperty().bind(NucleusGeneration.hadronRadius.divide(2));
                    Line bottom = new Line();
                    lightningBolt1.getChildren().add(bottom);
                    bottom.startXProperty().bind(lightningBolt1.widthProperty().multiply(0.5));
                    bottom.startYProperty().bind(lightningBolt1.heightProperty());
                    bottom.endXProperty().bind(lightningBolt1.widthProperty().multiply(0));
                    bottom.endYProperty().bind(lightningBolt1.heightProperty().multiply(0.4));

                    Line middle = new Line();
                    lightningBolt1.getChildren().add(middle);
                    middle.startXProperty().bind(lightningBolt1.widthProperty());
                    middle.startYProperty().bind(lightningBolt1.heightProperty().multiply(0.6));
                    middle.endXProperty().bind(lightningBolt1.widthProperty().multiply(0));
                    middle.endYProperty().bind(lightningBolt1.heightProperty().multiply(0.4));

                    Line top = new Line();
                    lightningBolt1.getChildren().add(top);
                    top.startXProperty().bind(lightningBolt1.widthProperty().multiply(0.5));
                    top.startYProperty().bind(lightningBolt1.widthProperty().multiply(0));
                    top.endXProperty().bind(lightningBolt1.widthProperty());
                    top.endYProperty().bind(lightningBolt1.heightProperty().multiply(0.6));
                }
                Pane lightningBolt2 = new Pane();
                {
                    lightningBolt2.prefHeightProperty().bind(NucleusGeneration.hadronRadius.multiply(2));
                    lightningBolt2.prefWidthProperty().bind(NucleusGeneration.hadronRadius.divide(2));
                    Line bottom = new Line();
                    lightningBolt2.getChildren().add(bottom);
                    bottom.startXProperty().bind(lightningBolt2.widthProperty().multiply(0.5));
                    bottom.startYProperty().bind(lightningBolt2.heightProperty());
                    bottom.endXProperty().bind(lightningBolt2.widthProperty().multiply(0));
                    bottom.endYProperty().bind(lightningBolt2.heightProperty().multiply(0.4));

                    Line middle = new Line();
                    lightningBolt2.getChildren().add(middle);
                    middle.startXProperty().bind(lightningBolt2.widthProperty());
                    middle.startYProperty().bind(lightningBolt2.heightProperty().multiply(0.6));
                    middle.endXProperty().bind(lightningBolt2.widthProperty().multiply(0));
                    middle.endYProperty().bind(lightningBolt2.heightProperty().multiply(0.4));

                    Line top = new Line();
                    lightningBolt2.getChildren().add(top);
                    top.startXProperty().bind(lightningBolt2.widthProperty().multiply(0.5));
                    top.startYProperty().bind(lightningBolt2.widthProperty().multiply(0));
                    top.endXProperty().bind(lightningBolt2.widthProperty());
                    top.endYProperty().bind(lightningBolt2.heightProperty().multiply(0.6));
                }
                Pane lightningBolt3 = new Pane();
                {
                    lightningBolt3.prefHeightProperty().bind(NucleusGeneration.hadronRadius.multiply(2));
                    lightningBolt3.prefWidthProperty().bind(NucleusGeneration.hadronRadius.divide(2));
                    Line bottom = new Line();
                    lightningBolt3.getChildren().add(bottom);
                    bottom.startXProperty().bind(lightningBolt3.widthProperty().multiply(0.5));
                    bottom.startYProperty().bind(lightningBolt3.heightProperty());
                    bottom.endXProperty().bind(lightningBolt3.widthProperty().multiply(0));
                    bottom.endYProperty().bind(lightningBolt3.heightProperty().multiply(0.4));

                    Line middle = new Line();
                    lightningBolt3.getChildren().add(middle);
                    middle.startXProperty().bind(lightningBolt3.widthProperty());
                    middle.startYProperty().bind(lightningBolt3.heightProperty().multiply(0.6));
                    middle.endXProperty().bind(lightningBolt3.widthProperty().multiply(0));
                    middle.endYProperty().bind(lightningBolt3.heightProperty().multiply(0.4));

                    Line top = new Line();
                    lightningBolt3.getChildren().add(top);
                    top.startXProperty().bind(lightningBolt3.widthProperty().multiply(0.5));
                    top.startYProperty().bind(lightningBolt3.widthProperty().multiply(0));
                    top.endXProperty().bind(lightningBolt3.widthProperty());
                    top.endYProperty().bind(lightningBolt3.heightProperty().multiply(0.6));
                }
                particlePane = new Pane(lightningBolt1,lightningBolt2,lightningBolt3);
                lightningBolt1.layoutXProperty().bind(lightningBolt2.layoutXProperty().subtract(lightningBolt1.widthProperty().multiply(1.4)));
                lightningBolt1.layoutYProperty().bind(particlePane.rotateProperty().multiply(0));
                lightningBolt2.layoutXProperty().bind(particlePane.rotateProperty().multiply(0));
                lightningBolt2.layoutYProperty().bind(particlePane.rotateProperty().multiply(0));
                lightningBolt3.layoutXProperty().bind(lightningBolt2.widthProperty().multiply(1.4));
                lightningBolt3.layoutYProperty().bind(particlePane.rotateProperty().multiply(0));
            }
        }
        this.type=type;
        FX.animateParticle(this,emitting,type);
    }
    public RadiationParticle(String type,boolean emitting) {
        //why does this need to be the first line? ternary statements it is then...
        this(
                ((type.equals("p")) ? 1 :
                        ((type.equals("n")) ? 2 :
                                ((type.equals("e")) ? 3 :
                                        ((type.equals("a")) ? 4 :
                                                ((type.equals("b")) ? 5 :
                                                    ((type.equals("g")) ? 6 :
                                                            0)))))),emitting);
    }
}
