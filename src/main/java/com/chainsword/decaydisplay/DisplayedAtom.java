package com.chainsword.decaydisplay;

import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.RotateTransition;
import javafx.beans.binding.DoubleBinding;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;

public class DisplayedAtom {
    private static int protonCount = 1;
    private static int neutronCount = 1;
    private static int electronCount = 1;
    public static void copyMainAtom() {
        setProtons(MainAtom.getProtons());
        setNeutrons(MainAtom.getProtons());
        setElectrons(MainAtom.getProtons());
    }
    public static int getCharge(){
        return protonCount-electronCount;
    }
    public static int getAtomicNumber(){
        return getProtons();
    }
    public static int getAtomicMass() { return getProtons()+getNeutrons();}
    public static int getProtons() {
        return protonCount;
    }
    public static void setProtons(int protons) {
        if (protons<0) {
            Error.negativeParticleError();
        }
        protonCount = Math.max(0,protons);
    }
    public static void addProtons(int protonsToAdd) {
        setProtons(getProtons()+protonsToAdd);
    }
    public static int getNeutrons() {
        return neutronCount;
    }
    public static void setNeutrons(int neutrons) {
        if (neutrons<0) {
            Error.negativeParticleError();
        }
        neutronCount = Math.max(0,neutrons);
    }
    public static void addNeutrons(int neutronsToAdd) {
        setNeutrons(getNeutrons()+neutronsToAdd);
    }
    public static int getElectrons() {
        return electronCount;
    }
    public static void setElectrons(int electrons) {
        if (electrons<0) {
            Error.negativeParticleError();
        }
        electronCount = Math.max(0,electrons);
    }
    public static void addElectrons( int electronsToAdd) {
        setElectrons(getElectrons()+electronsToAdd);
    }
    public static double getAtomicWeight(){
        return (protonCount*1.00727647)+(neutronCount*1.008665)+(electronCount*0.0005489);
    }
    public static void radiation(int type, boolean emitting) {
        switch (type) {
            case 1 -> {if (emitting) {emitProton();  } else { absorbProton();  }}
            case 2 -> {if (emitting) {emitNeutron(); } else { absorbNeutron(); }}
            case 3 -> {if (emitting) {emitElectron();} else { absorbElectron();}}
            case 4 -> {if (emitting) {emitAlpha();   } else { unEmitAlpha();   }}
            case 5 -> {if (emitting) {emitBeta();    } else { unEmitBeta();    }}
            case 6 -> {if (emitting) {emitGamma();   } else { unEmitGamma();     }}
        }
    }
    public static void unEmitAlpha() {
        addProtons(2);
        addNeutrons(2);
    }
    public static void emitAlpha() {
        addProtons(-2);
        addNeutrons(-2);
    }
    public static void unEmitBeta() {
        addProtons(-1);
        addNeutrons(1);
    }
    public static void emitBeta() {
        addProtons(1);
        addNeutrons(-1);
    }
    public static void unEmitGamma() {
    }
    public static void emitGamma() {
    }
    public static void absorbProton() {
        addProtons(1);
    }
    public static void emitProton() {
        addProtons(-1);
    }
    public static void absorbNeutron() {
        addNeutrons(1);
    }
    public static void emitNeutron() {
        addNeutrons(-1);
    }
    public static void absorbElectron() {
        addElectrons(1);
    }
    public static void emitElectron() {
        addElectrons(-1);
    }
    static final Circle center = new Circle(1, Color.BLACK);
    static Pane nucleusPane = new Pane();
    static Pane electronPane = new Pane();
    static final Pane mainPane = new Pane(nucleusPane,electronPane, center);

    public static Pane generateBohrModel() {

        // Calculate the radius difference between each shell based on the number of shells needed and the width of the Pane
        DoubleBinding gapSizeScale = mainPane.heightProperty().multiply(1 /(double)(PeriodicTable.likelyNumShells(MainAtom.getElectrons()) + 2));

        // recreate the panes to hold the Bohr model
        mainPane.getChildren().remove(nucleusPane);
        nucleusPane = new Pane();
        mainPane.getChildren().add(nucleusPane);
        nucleusPane.prefWidthProperty().bind(gapSizeScale);
        nucleusPane.prefHeightProperty().bind(gapSizeScale);
        nucleusPane.layoutXProperty().bind(mainPane.widthProperty().subtract(nucleusPane.widthProperty()).divide(2));
        nucleusPane.layoutYProperty().bind(mainPane.heightProperty().subtract(nucleusPane.heightProperty()).divide(2));
        mainPane.getChildren().remove(electronPane);
        electronPane = new Pane();
        mainPane.getChildren().add(electronPane);
        electronPane.prefWidthProperty().bind(mainPane.widthProperty());
        electronPane.prefHeightProperty().bind(mainPane.heightProperty());
        electronPane.layoutXProperty().bind(mainPane.widthProperty().multiply(0));
        electronPane.layoutYProperty().bind(mainPane.heightProperty().multiply(0));
        //nucleus
        {
            // Create a Circle for the back of the nucleus and add it to the Pane
            Circle nucleus = new Circle();
            nucleus.setFill(Color.GREY);
            nucleus.setStroke(Color.BLACK);
            nucleusPane.getChildren().add(nucleus);
            nucleus.radiusProperty().bind(nucleusPane.widthProperty().divide(2));
            nucleus.centerXProperty().bind(nucleusPane.widthProperty().divide(2));
            nucleus.centerYProperty().bind(nucleusPane.heightProperty().divide(2));
            //int totalLeft = getNeutrons()+getProtons();
            //int neutronsLeft = getNeutrons();
            //int protonsLeft = getProtons();

            // Calculate the size of neutrons and protons based on the amount that must fit in the nucleus, and the size of the nucleus
            NucleusGeneration.assembleNucleus(MainAtom.getNeutrons(), MainAtom.getProtons(), nucleusPane);

            int rotationsPerCycle = 1;
            RotateTransition rotator = new RotateTransition(Duration.minutes((1 + Math.random() / 4) * rotationsPerCycle), nucleusPane);
            rotator.setCycleCount(Animation.INDEFINITE);
            rotator.setByAngle(-360);
            rotator.setAutoReverse(false);
            rotator.setInterpolator(Interpolator.LINEAR);
            /*rotator.setFromAngle(0);
            rotator.setToAngle(360*rotationsPerCycle);
            rotator.onFinishedProperty().set(actionEvent -> rotator.playFromStart());*/
            rotator.play();
            // make this a valid thing...

            //NucleusGeneration.hadronRadius.bind(nucleusPane.widthProperty().divide((getProtons() + getNeutrons())));
            // Calculate the number of electron shells needed to represent the given number of electrons, first with the actual numbers, and then with a formula I derived from them.
            // circle packing is hard... I'm going to make a new class specifically for dealing with this...

            /*
            // Add a Circle for each neutron in the nucleus
            for (int i = 0; i < getNeutrons(); i++) {
                // Calculate the position of the neutron on the nucleus based on its position in the loop
                //double angle = i * (360.0 / getNeutrons());
                DoubleBinding x = nucleus.centerXProperty().add(nucleus.radiusProperty().subtract(10).multiply(Math.cos(Math.toRadians(i * (360.0 / getNeutrons())))));
                DoubleBinding y = nucleus.centerYProperty().add(nucleus.radiusProperty().subtract(10).multiply(Math.sin(Math.toRadians(i * (360.0 / getNeutrons())))));

                // Add a Circle for the neutron
                Circle neutron = new Circle();
                makeNeutron(neutron);
                nucleusPane.getChildren().add(neutron);
                neutron.centerXProperty().bind(x);
                neutron.centerYProperty().bind(y);
                neutron.radiusProperty().bind(NucleusGeneration.hadronRadius);
            }

            // Add a Circle for each proton in the nucleus
            for (int i = 0; i < getProtons(); i++) {
                // Calculate the position of the proton on the nucleus based on its position in the loop
                double angle = i * (360.0 / getProtons());
                DoubleBinding x = nucleus.centerXProperty().add(nucleus.radiusProperty().subtract(10).multiply(Math.cos(Math.toRadians(angle))));
                DoubleBinding y = nucleus.centerYProperty().add(nucleus.radiusProperty().subtract(10).multiply(Math.sin(Math.toRadians(angle))));

                // Add a Circle for the proton
                Circle proton = new Circle();
                makeProton(proton);
                nucleusPane.getChildren().add(proton);
                proton.centerXProperty().bind(x);
                proton.centerYProperty().bind(y);
            }*/
        }
        //electrons
        {
            int electronsLeft = MainAtom.getElectrons();
            // Create a Pane and Circle for each shell and add it to the main electron Pane, and then make as many electrons as is expected in that shell
            for (int i = 0; i < PeriodicTable.likelyNumShells(MainAtom.getElectrons()); i++) {
                Pane electronRing = new Pane();
                electronPane.getChildren().add(electronRing);
                electronRing.prefWidthProperty().bind(gapSizeScale.multiply(i + 2));
                electronRing.prefHeightProperty().bind(gapSizeScale.multiply(i + 2));
                electronRing.layoutXProperty().bind(electronPane.widthProperty().subtract(electronRing.widthProperty()).divide(2));
                electronRing.layoutYProperty().bind(electronPane.heightProperty().subtract(electronRing.heightProperty()).divide(2));
                Circle ring = new Circle();
                electronRing.getChildren().add(ring);
                ring.setFill(null);
                ring.setStroke(Color.BLACK);
                ring.radiusProperty().bind(electronRing.widthProperty().divide(2));
                ring.centerXProperty().bind(electronRing.widthProperty().divide(2));
                ring.centerYProperty().bind(electronRing.heightProperty().divide(2));
                int electronsInThisShell = Math.min(electronsLeft, PeriodicTable.electronsInShell(i + 1));
                double angleBetweenElectronsInThisShell = Math.toRadians( 360d / (double)electronsInThisShell);
                electronsLeft -= electronsInThisShell;
                for (int j = 0; j < electronsInThisShell; j++) {
                /*DoubleBinding angle = electronRing.rotateProperty().add(angleBetweenElectronsInThisShell*(i+1));
                DoubleBinding x = ring.radiusProperty().multiply(Bindings.createDoubleBinding(() -> Math.cos(angle.get()), angle));
                DoubleBinding y = ring.radiusProperty().multiply(Bindings.createDoubleBinding(() -> Math.sin(angle.get()), angle));*/
                    double angle = angleBetweenElectronsInThisShell * (j + 1);
                    DoubleBinding x = ring.radiusProperty().multiply(Math.cos(angle)).add(ring.radiusProperty());
                    DoubleBinding y = ring.radiusProperty().multiply(Math.sin(angle)).add(ring.radiusProperty());
                    Circle electron = new Circle();
                    electronRing.getChildren().add(electron);
                    NucleusGeneration.makeElectron(electron);
                    electron.centerXProperty().bind(x);
                    electron.centerYProperty().bind(y);
                    //System.out.println(x.doubleValue() + String.valueOf(Math.cos(angle)) + "," + y.doubleValue() + Math.sin(angle) + " :" + angle);
                }
                int rotationsPerCycle = 1;
                RotateTransition rotator = new RotateTransition(Duration.minutes((1 + Math.random() / 4) * rotationsPerCycle), electronRing);
                rotator.setCycleCount(Animation.INDEFINITE);
                rotator.setByAngle(360);
                rotator.setAutoReverse(false);
                rotator.setInterpolator(Interpolator.LINEAR);
            /*rotator.setFromAngle(0);
            rotator.setToAngle(360*rotationsPerCycle);
            rotator.onFinishedProperty().set(actionEvent -> rotator.playFromStart());*/
                rotator.play();
            }
        }
        return mainPane;
    }
    /*public static Pane generateNucleus() {
        nucleusPane = new Pane();
        for (int i = 0; i < getProtons(); i++) {
            Circle proton = new Circle(FX.centerSquarePane.widthProperty().divide(NucleusGeneration.hadronRadius).doubleValue());
            NucleusGeneration.makeProton(proton);
            proton.radiusProperty().bind(FX.centerSquarePane.widthProperty().divide(NucleusGeneration.hadronRadius));

            proton.centerXProperty().bind(nucleusPane.widthProperty().multiply(0.5).subtract(FX.centerSquarePane.widthProperty().divide(NucleusGeneration.hadronRadius).doubleValue()));
            proton.centerXProperty().bind(nucleusPane.heightProperty().multiply(0.5).subtract(FX.centerSquarePane.widthProperty().divide(NucleusGeneration.hadronRadius).doubleValue()));
            nucleusPane.getChildren().add(proton);

            if (getNeutrons()>i) {

                Circle neutron = new Circle(FX.centerSquarePane.widthProperty().divide(NucleusGeneration.hadronRadius).doubleValue()));
                neutron.radiusProperty().bind(FX.centerSquarePane.widthProperty().divide(NucleusGeneration.hadronRadius));

                neutron.centerXProperty().bind(nucleusPane.widthProperty().multiply(0.5).subtract(FX.centerSquarePane.widthProperty().divide(NucleusGeneration.hadronRadius).doubleValue()));
                neutron.centerXProperty().bind(nucleusPane.heightProperty().multiply(0.5).subtract(FX.centerSquarePane.widthProperty().divide(NucleusGeneration.hadronRadius).doubleValue()));
                NucleusGeneration.makeNeutron(neutron);
                nucleusPane.getChildren().add(neutron);
            }
        }
        return nucleusPane;
    }*/
}