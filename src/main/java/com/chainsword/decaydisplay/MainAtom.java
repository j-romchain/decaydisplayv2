package com.chainsword.decaydisplay;// **********************************************************************************
// Title: the particle class
// Author: Jarom Wright
// Course Section: CMIS201-ONL3 (Seidel) Spring 2023
// File: Particle.java
// Description: a class for keeping track of all the Particles on the screen. Particles constitute all the floating things involved with the simulation. this class holds SubParticles that are shown clustered together on the screen.
// **********************************************************************************


@SuppressWarnings("EmptyMethod")
public class MainAtom {
    private static int protonCount = 1;
    private static int neutronCount = 1;
    private static int electronCount = 1;
    public static void setMainAtom(int protons, int neutrons, int electrons) {
        setProtons(protons);
        setNeutrons(neutrons);
        setElectrons(electrons);
    }
    public static void setMainAtom(int atomicNumber) {
        setMainAtom(atomicNumber,PeriodicTable.mostLikelyNeutronCount(atomicNumber), atomicNumber);
    }
    public static String toSave() {
        return "loadSave{" +
                "protons=" + getProtons() +
                ", neutrons="+ getNeutrons() +
                ", electrons="+ getElectrons() +
                '}';
    }
    public static void loadSave(String saveText){
        saveText=saveText.substring(saveText.indexOf("protons=")+8);
        setProtons(Integer.parseInt(saveText.substring(0,saveText.indexOf(","))));
        saveText=saveText.substring(saveText.indexOf("neutrons=")+9);
        setNeutrons(Integer.parseInt(saveText.substring(0,saveText.indexOf(","))));
        saveText=saveText.substring(saveText.indexOf("electrons=")+10);
        setElectrons(Integer.parseInt(saveText.substring(0,saveText.indexOf("}"))));
    }
    public static String string() {
        return "\nloadSave{" +
                "\nProtons=" + getProtons() +
                ", \nNeutrons="+ getNeutrons() +
                ", \nElectrons=" + getElectrons() +
                ", \nCharge=" + getCharge() +
                ", \nAtomicNumber=" + getAtomicNumber() +
                ", \nAtomicMass=" + getAtomicMass() +
                ", \nAtomicWeight=" + getAtomicWeight() +
                ", \nNeutronCountBasedOnProtons=" + PeriodicTable.mostLikelyNeutronCount(getProtons()) +
                ", \n" +
                "\n}";
    }

    public static int getCharge(){
        return protonCount-electronCount;
    }
    public static int getAtomicNumber(){
        return getProtons();
    }
    public static int getAtomicMass() { return getProtons()+getNeutrons();}
    public static int getProtons() {
        return protonCount;
    }
    public static void setProtons(int protons) {
        if (protons<0) {
            Error.negativeParticleError();
        }
        protonCount = Math.max(0,protons);
    }
    public static void addProtons(int protonsToAdd) {
        setProtons(getProtons()+protonsToAdd);
    }
    public static int getNeutrons() {
        return neutronCount;
    }
    public static void setNeutrons(int neutrons) {
        if (neutrons<0) {
            Error.negativeParticleError();
        }
        neutronCount = Math.max(0,neutrons);
    }
    public static void addNeutrons(int neutronsToAdd) {
        setNeutrons(getNeutrons()+neutronsToAdd);
    }
    public static int getElectrons() {
        return electronCount;
    }
    public static void setElectrons(int electrons) {
        if (electrons<0) {
            Error.negativeParticleError();
        }
        electronCount = Math.max(0,electrons);
    }
    public static void addElectrons( int electronsToAdd) {
        setElectrons(getElectrons()+electronsToAdd);
    }
    public static double getAtomicWeight(){
        return (protonCount*1.00727647)+(neutronCount*1.008665)+(electronCount*0.0005489);
    }
    public static void radiation(int type, boolean emitting) {
        switch (type) {
            case 1 -> {if (emitting) {emitProton();  } else { absorbProton();  }}
            case 2 -> {if (emitting) {emitNeutron(); } else { absorbNeutron(); }}
            case 3 -> {if (emitting) {emitElectron();} else { absorbElectron();}}
            case 4 -> {if (emitting) {emitAlpha();   } else { unEmitAlpha();   }}
            case 5 -> {if (emitting) {emitBeta();    } else { unEmitBeta();    }}
            case 6 -> {if (emitting) {emitGamma();   } else { unEmitGamma();     }}
        }
    }
    public static void unEmitAlpha() {
        addProtons(2);
        addNeutrons(2);
    }
    public static void emitAlpha() {
        addProtons(-2);
        addNeutrons(-2);
    }
    public static void unEmitBeta() {
        addProtons(-1);
        addNeutrons(1);
    }
    public static void emitBeta() {
        addProtons(1);
        addNeutrons(-1);
    }
    public static void unEmitGamma() {
    }
    public static void emitGamma() {
    }
    public static void absorbProton() {
        addProtons(1);
    }
    public static void emitProton() {
        addProtons(-1);
    }
    public static void absorbNeutron() {
        addNeutrons(1);
    }
    public static void emitNeutron() {
        addNeutrons(-1);
    }
    public static void absorbElectron() {
        addElectrons(1);
    }
    public static void emitElectron() {
        addElectrons(-1);
    }
}
