package com.chainsword.decaydisplay;

// **********************************************************************************
// Title: DecayDisplay (main)
// Author: Jarom Wright
// Course Section: CMIS201-ONL3 (Seidel) Spring 2023
// File: main.java
// Description: the main file for my DecayDisplay project.
// **********************************************************************************
public class Main {
    public static boolean GUIInit = false;
    public static void main(String[] args) {
        System.out.println("DecayDisplay, by ChainSword20000.");
        if (args.length>0) {
        //check if there are arguments
            if (args[0].toLowerCase().contains("a")||args[0].toLowerCase().contains("c")) {
            //check if the first argument contains c or a
                var argOne=args[0].toLowerCase();
                if (argOne.contains("a")&&argOne.contains("c")) {
                //check if its just one or if we need to see which one comes first
                    if (argOne.indexOf("a")<argOne.indexOf("c")){
                    //check if "a" was first
                        System.out.println(API(args));
                    } else {
                    //c must be first
                        CLI();
                    }
                } else if (argOne.contains("a")) {
                //check if the one is a
                    System.out.println(API(args));
                } else {
                //the one must be c
                    CLI();
                }
            } else {
                Error.argumentCompleteError();
                GUI(args);
            }
        } else {
            GUI(args);
        }
    }
    public static void GUI(String[] args) {
        //noinspection StatementWithEmptyBody
        if (GUIInit) {
            //tests, anything else should be in the start function in the FX class because this and run cannot be guaranteed to be run when the file is double-clicked.
            //new RadiationParticle("p", true);
            //new RadiationParticle("n", false);
            //new RadiationParticle("a", true);
            //new RadiationParticle("g", false);
            //new RadiationParticle("e", true);
            //System.out.println(((Circle)MainAtom.nucleusPane.getChildren().get(1)).getRadius());
            //FX.setMainAtomParticle(mainAtom);
            //MainAtom tmp = new setMainAtom(2,2,0);
            //tmp.toFlyingParticle();
            //FX.animateParticle(tmp);
            //System.out.println(((Pane)FX.centerDisposablePane.getChildren().get(0)).getChildren());
        } else {
            FX.run(args);
        }
    }
    public static String API(String[] args) {
        args[0]="is done";
        if (args.length>2) {
            if (args[1].equals("#")) {
                MainAtom.setMainAtom(Integer.parseInt(args[2]));
                args[1]="is done";
                args[2]="is done";
            } else if (args[1].contains("/") && args[1].length()>1) {
                Save.load(args[1]);
                args[1]=args[1]+"is done";
            } else if (args[1].equals("/")) {
                Save.load();
                args[1]="is done";
            } else if (args.length>5 && args[1].equalsIgnoreCase("pne")) {
                MainAtom.setMainAtom(Integer.parseInt(args[2]),Integer.parseInt(args[3]),Integer.parseInt(args[4]));
                args[1]="is done";
                args[2]="is done";
                args[3]="is done";
                args[4]="is done";

            } else {
                Error.argumentPartialError();
            }
            //System.out.println("loaded main atom:" + mainAtom);
            for (String argument: args) {
                if (!argument.contains("done")) {
                    argument=argument.toLowerCase();
                    switch (argument) {
                        case "a", "-a" -> MainAtom.emitAlpha();
                        case "+a" -> MainAtom.unEmitAlpha();
                        case "b", "-b" -> MainAtom.emitBeta();
                        case "+b" -> MainAtom.unEmitBeta();
                        case "g", "-g" -> MainAtom.emitGamma();
                        case "+g" -> MainAtom.unEmitGamma();
                        case "p", "-p" -> MainAtom.emitProton();
                        case "+p" -> MainAtom.absorbProton();
                        case "n", "-n" -> MainAtom.emitNeutron();
                        case "+n" -> MainAtom.absorbNeutron();
                        case "e", "-e" -> MainAtom.emitElectron();
                        case "+e" -> MainAtom.absorbElectron();
                        default -> Error.argumentPartialError();
                    }
                }
            }
            if (args[1].contains("/")) {
                Save.save(args[1].substring(0,args[1].lastIndexOf("is")));
            } else Save.save();
        } else {
            Error.argumentCompleteError();
        }
        return MainAtom.string();
    }
    public static void CLI() {
        //TODO make the CLI work

    }

    public static void test() {
        System.out.println(API(new String[]{"a","pne","5","6","7","a","b","+a","+b"}));
        MainAtom.setMainAtom(5,6,7);
        System.out.println(MainAtom.toSave());
        MainAtom.loadSave(MainAtom.toSave());
        System.out.println(MainAtom.string());
        System.out.println(API(new String[]{"a","/","a","b","+a","+b"}));
    }
}
