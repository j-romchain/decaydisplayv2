package com.chainsword.decaydisplay;

// **********************************************************************************
// Title: DecayDisplay
// Author: Jarom Wright
// Course Section: CMIS201-ONL3 (Seidel) Spring 2023
// File: Test.java
// Description: a file so that I can test the program.
// **********************************************************************************
public class Test {
    public static void main(String[] args) {
        Main.test();
    }
}
