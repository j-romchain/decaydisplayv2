package com.chainsword.decaydisplay;

//import org.json.*;

// **********************************************************************************
// Title: Periodic Table
// Author: Jarom Wright
// Course Section: CMIS201-ONL3 (Seidel) Spring 2023
// File: PeriodicTable.java
// Description: a static class that contains information pulled from the periodic table.
// **********************************************************************************
public class PeriodicTable {
    static final String[] names = {"H - Hydrogen", "He - Helium", "Li - Lithium", "Be - Beryllium", "B - Boron", "C - Carbon", "N - Nitrogen", "O - Oxygen", "F - Fluorine", "Ne - Neon", "Na - Sodium", "Mg - Magnesium", "Al - Aluminum", "Si - Silicon", "P - Phosphorus", "S - Sulfur", "Cl - Chlorine", "Ar - Argon", "K - Potassium", "Ca - Calcium", "Sc - Scandium", "Ti - Titanium", "V - Vanadium", "Cr - Chromium", "Mn - Manganese", "Fe - Iron", "Co - Cobalt", "Ni - Nickel", "Cu - Copper", "Zn - Zinc", "Ga - Gallium", "Ge - Germanium", "As - Arsenic", "Se - Selenium", "Br - Bromine", "Kr - Krypton", "Rb - Rubidium", "Sr - Strontium", "Y - Yttrium", "Zr - Zirconium", "Nb - Niobium", "Mo - Molybdenum", "Tc - Technetium", "Ru - Ruthenium", "Rh - Rhodium", "Pd - Palladium", "Ag - Silver", "Cd - Cadmium", "In - Indium", "Sn - Tin", "Sb - Antimony", "Te - Tellurium", "I - Iodine", "Xe - Xenon", "Cs - Cesium", "Ba - Barium", "La - Lanthanum", "Ce - Cerium", "Pr - Praseodymium", "Nd - Neodymium", "Pm - Promethium", "Sm - Samarium", "Eu - Europium", "Gd - Gadolinium", "Tb - Terbium", "Dy - Dysprosium", "Ho - Holmium", "Er - Erbium", "Tm - Thulium", "Yb - Ytterbium", "Lu - Lutetium", "Hf - Hafnium", "Ta - Tantalum", "W - Tungsten", "Re - Rhenium", "Os - Osmium", "Ir - Iridium", "Pt - Platinum", "Au - Gold", "Hg - Mercury", "Tl - Thallium", "Pb - Lead", "Bi - Bismuth", "Po - Polonium", "At - Astatine", "Rn - Radon", "Fr - Francium", "Ra - Radium", "Ac - Actinium", "Th - Thorium", "Pa - Protactinium", "U - Uranium", "Np - Neptunium", "Pu - Plutonium", "Am - Americium", "Cm - Curium", "Bk - Berkelium", "Cf - Californium", "Es - Einsteinium", "Fm - Fermium", "Md - Mendelevium", "No - Nobelium", "Lr - Lawrencium", "Rf - Rutherfordium", "Db - Dubnium", "Sg - Seaborgium", "Bh - Bohrium", "Hs - Hassium", "Mt - Meitnerium", "Ds - Darmstadtium", "Rg - Roentgenium", "Cn - Copernicium", "Nh - Nihonium", "Fl - Flerovium", "Mc - Moscovium", "Lv - Livermorium", "Ts - Tennessine", "Og - Oganesson"};
    static final String[] classes = {"Non Metal", "Noble Gas", "Alkali Metal", "Alkaline Earth Metal", "Metalloids", "Non Metal", "Non Metal", "Non Metal", "Halogen", "Noble Gas", "Alkali Metal", "Alkaline Earth Metal", "Poor Metal", "Metalloids", "Non Metal", "Non Metal", "Halogen", "Noble Gas", "Alkali Metal", "Alkaline Earth Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Poor Metal", "Metalloids", "Metalloids", "Non Metal", "Halogen", "Noble Gas", "Alkali Metal", "Alkaline Earth Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Poor Metal", "Poor Metal", "Metalloids", "Metalloids", "Halogen", "Noble Gas", "Alkali Metal", "Alkaline Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Poor Metal", "Poor Metal", "Poor Metal", "Metalloids ?", "Metalloids", "Noble Gas", "Alkali Metal", "Alkaline Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Rare Earth Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Transition Metal", "Transition Metal ?", "Transition Metal ?", "Transition Metal ?", "Transition Metal", "Post-Transition Metal ?", "Post-Transition Metal ?", "Post-Transition Metal ?", "Post-Transition Metal ?", "Post-Transition Metal ?", "Noble Gas ?"};
    static final Integer[] neutrons = {0, 2, 3, 5, 5, 6, 7, 7, 9, 10, 11, 12, 13, 14, 15, 16, 18, 21, 20, 20, 23, 25, 27, 27, 29, 29, 31, 30, 34, 35, 38, 40, 41, 44, 44, 47, 48, 49, 49, 51, 51, 53, 55, 57, 57, 60, 60, 64, 65, 68, 70, 75, 73, 77, 77, 81, 81, 82, 81, 84, 84, 88, 88, 93, 93, 96, 97, 99, 99, 103, 103, 106, 107, 109, 111, 114, 115, 117, 117, 120, 123, 125, 125, 125, 125, 136, 136, 138, 138, 142, 140, 146, 144, 150, 148, 151, 150, 153, 153, 157, 157, 157, 159, 157, 157, 160, 157, 169, 159, 171, 161, 173, 173, 175, 175, 176, 177, 176};
    static int neutronCount(int protonCount){
        if (neutrons.length>=protonCount&&protonCount>=1) {
            return neutrons[protonCount-1];
        } else {
            return -1;
        }
    }
    static int mostLikelyNeutronCount(int protonCount){
        if (neutrons.length>=protonCount&&protonCount>=1) {
            return neutrons[protonCount-1];
        } else {
            return (int)(1.7*protonCount - 18);
        }
    }
    static int likelyNumShells(int electrons) {
        return(
                electrons <= 2 ? 1 :
                        electrons <= 10 ? 2 :
                                electrons <= 18 ? 3 :
                                        electrons <= 36 ? 4 :
                                                electrons <= 55 ? 5 :
                                                        electrons <= 86 ? 6 :
                                                                electrons <= 118 ? 7 :
                                                                        likelyNumShellsBackend(electrons));

    }
    private static int likelyNumShellsBackend(int electrons) {
        int shells = 1;
        while (electrons>0) {
            electrons-=electronsInShell(shells);
            shells +=1;
        }
        return shells-1;
    }
    static int electronsInShell(int shells) {
        return (
                shells <= 1 ? 2 :
                        shells == 2 ? 8 :
                                shells == 3 ? 8 :
                                        shells == 4 ? 18 :
                                                shells == 5 ? 18 :
                                                        shells == 6 ? 32 :
                                                                shells == 7 ? 32 :
                                                                        //todo ?fix this?
                                                                        (int) Math.pow(((int) Math.floor(shells / 2d) * 2 + 2) / 2.0, 2) * 2);
    }
    public static String getName(int atomicNumber) {
        if (names.length>=atomicNumber&&atomicNumber>=1) {
            return names[atomicNumber-1];
        } else {
            return "Not Applicable";
        }
    }
    public static String classOf(int atomicNumber) {
        if (classes.length>=atomicNumber&&atomicNumber>=1) {
            return classes[atomicNumber-1];
        } else {
            return "Unknown";
        }
    }
}