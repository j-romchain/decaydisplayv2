package com.chainsword.decaydisplay;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

// **********************************************************************************
// Title: DecayDisplay
// Author: Jarom Wright
// Course Section: CMIS201-ONL3 (Seidel) Spring 2023
// File: Save.java
// Description: a class for saving too and loading from the save file.
// **********************************************************************************
public class Save {
    public static void save(String saveFile) {
        File file = new File(saveFile);
        if (file.exists()) //noinspection ResultOfMethodCallIgnored
            file.delete();
        PrintWriter fileWriter= null;
        try {
            fileWriter = new PrintWriter(file);
        } catch (FileNotFoundException e) {
            Error.saveError();
            //System.out.println("ERROR: referenced save couldn't be made or couldn't be found... this may cause big errors, this may not cause any errors...");
        }
        assert fileWriter != null;
        fileWriter.println(MainAtom.toSave());
        fileWriter.close();
    }
    public static void save() {
        save("./DecayDisplay.save");
    }
    public static void load(String saveFile) {
        File file = new File(saveFile);
        if (!file.exists()) {Error.loadError(); MainAtom.setMainAtom(0,0,0);save(saveFile);}
        Scanner fileScanner = null;
        try {
            fileScanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            Error.loadError();
        }
        assert fileScanner != null;
        MainAtom.loadSave(fileScanner.nextLine());
    }
    public static void load() {
        load("./DecayDisplay.save");
    }
}
