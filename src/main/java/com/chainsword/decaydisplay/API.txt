the program accepts arguments in the format:
[program name] [mode] [start atom format indicator] [start atom format] [instructions]
where:
[program name] doesn't reach the program as an argument, and is ignored
[mode] isn't required, and can be anything containing a,A,c,C, any combination of them, or it and all subsequent arguments not be provided.
    In the case where it and all other arguments are not provided it defaults to GUI mode.
    In the case that it contains an 'a' ignoring capitalization within the argument first it will enter API mode and other arguments are interpreted.
    In the case that it contains a 'c' ignoring capitalization within the argument first it will enter CLI mode and other arguments are ignored.

    if the API is run, the argument format starts with a set of arguments to set the initial atom, followed by a set of arguments that set the modifications that occur to the atom.

    the arguments that set the initial atom start with a format indicator, either a PNE for a manual declaration, a # for a copy from the periodic table, or a file path to load from a save file.
    the save file path must contain / or \ and be a full path to the file relative to [the programs location, or was it the running directory], [one part of the path][/ or \][valid save file full name]
    the manual declaration must be 3 integers with the start being proton count, the middle being the neutron count, and the last being the electron count, pne [p] [n] [e]
    the periodic table copy must be a valid atomic number and starts with a balanced charge and the most common neutron count as found on the periodic table or a count based
        on a function of regression for the neutron count most likely to be stable were it not for the electromagnetic force overpowering gravity and the strong force, based on a function of best fit.

    the modification arguments consist of one of these characters: a,b,d,p,n,e, and may start with a + or -, though the - is ignored. the characters stand for alpha, beta, and gamma radiation, proton, neutron, and electron,
        and unless it starts with a + the atom will emit the mainAtom or undergo the radiation, and if it starts with a + the atom will absorb the mainAtom, or un-undergo the radiation.