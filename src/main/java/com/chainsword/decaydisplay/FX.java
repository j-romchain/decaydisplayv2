package com.chainsword.decaydisplay;

import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.Border;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Transform;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.ArrayList;

// **********************************************************************************
// Title: FX class
// Author: Jarom Wright
// Course Section: CMIS201-ONL3 (Seidel) Spring 2023
// File: FX.java
// Description: the application class, run by GUI() in Main and consisting of the entire graphical user interface management involving javaFX except for the particles...
// **********************************************************************************
public class FX extends Application {
    //Mark - these are the static declarations - declared as null because when the class loads, the display hasn't been initialized yet.
    static Label numberTitle, manualTitle, protonTitle, neutronTitle, electronTitle, fileTitle, nameTitle, atomicNumberTitle, classTitle, neutronEstimateTitle,
            electronCountTitle, chargeTitle, weightTitle, protonCountTitle, neutronCountTitle, radiationTitle;
    static TextField numberBox, protonBox, neutronBox, electronBox, fileLocationBox, nameBox, atomicNumberBox, classBox, neutronEstimateBox, chargeBox, weightBox,
            protonCountBox, electronCountBox, neutronCountBox;
    static Button numberDefinitionBtn, manualDefinitionBtn, loadBtn, saveBtn, addProtonBtn, remProtonBtn, addNeutronBtn, remNeutronBtn, addElectronBtn,
            remElectronBtn, alphaEmitBtn, alphaAbsBtn, betaEmitBtn, betaAbsBtn, gammaEmitBtn, gammaAbsBtn;
    static Pane numberDefinitionPane, manualDefinitionPane, protonDefinitionPane, neutronDefinitionPane, electronDefinitionPane, filePane, leftPane, centerSquarePane, centerPane,
            infoPane, PNEPane, radiationPane, rightPane, mainPane;
    static Slider speed;
    static final ArrayList<Transition> animations = new ArrayList<>();


    //Mark - these are the methods used for starting the display
    public static void main(String[] args){
        Main.main(args);
    }
    public static void run(String[] args) {
        Application.launch(args);
    }
    @Override // Override the start method in the Application class
    public void start(Stage primaryStage) {

        setAndPositionElements();
        colorize();
        Rectangle2D screenSize = Screen.getPrimary().getVisualBounds();
        Scene mainScene = new Scene(mainPane, screenSize.getMaxX() * 0.5, screenSize.getMaxX() * 0.5 *0.6);
        primaryStage.setTitle("DecayDisplay"); // Set the stage title
        primaryStage.setScene(mainScene); // Place the scene in the stage
        setupEventHandlers();
        updateAll();
        Main.GUIInit = true;
        primaryStage.show(); // Display the stage
        Main.GUI(new String[0]);
    }

    //Mark - these are used to set up the default scene, including everything except the contents of centerSquarePane - which is used to animate and display the main atom.
    public void colorize() {
        mainPane.setBorder(Border.stroke(Color.BLACK));
        {
            leftPane.setBorder(Border.stroke(Color.BLACK));
            {
                filePane.setBorder(Border.stroke(Color.GREY));
                manualDefinitionPane.setBorder(Border.stroke(Color.GREY));
                {
                    protonDefinitionPane.setBorder(Border.stroke(Color.LIGHTGREY));
                    neutronDefinitionPane.setBorder(Border.stroke(Color.LIGHTGREY));
                    electronDefinitionPane.setBorder(Border.stroke(Color.LIGHTGREY));
                }
                numberDefinitionPane.setBorder(Border.stroke(Color.GREY));

            }
            centerPane.setBorder(Border.stroke(Color.BLACK));
            centerPane.setBackground(Background.fill(Color.LIGHTGREY));
            {
                centerSquarePane.setBorder(Border.stroke(Color.GREY));
                centerSquarePane.setBackground(Background.fill(Color.WHITE));
            }
            rightPane.setBorder(Border.stroke(Color.BLACK));
            {
                infoPane.setBorder(Border.stroke(Color.GREY));
                PNEPane.setBorder(Border.stroke(Color.GREY));
                radiationPane.setBorder(Border.stroke(Color.GREY));
            }
        }
    }
    public void setAndPositionElements() {
        //mainPane is full size
        mainPane = new Pane();
        //mainPane.prefWidthProperty().bind(mainScene.widthProperty());
        //mainPane.prefHeightProperty().bind(mainScene.heightProperty());
        //leftPane is one fifth of the width of the screen
        {
            centerPane = new Pane();
            mainPane.getChildren().add(centerPane);
            centerPane.layoutXProperty().bind(mainPane.widthProperty().subtract(Bindings.min(mainPane.widthProperty().multiply(0.6),mainPane.heightProperty())).multiply(0.5));
            centerPane.prefWidthProperty().bind(Bindings.min(mainPane.widthProperty().multiply(0.6),mainPane.heightProperty()));
            centerPane.prefHeightProperty().bind(mainPane.heightProperty());
            {
                centerSquarePane = new Pane();
                centerPane.getChildren().add(centerSquarePane);
                centerSquarePane.prefWidthProperty().bind(Bindings.min(centerPane.heightProperty(),centerPane.widthProperty()));
                centerSquarePane.prefHeightProperty().bind(Bindings.min(centerPane.heightProperty(),centerPane.widthProperty()));
                centerSquarePane.layoutXProperty().bind(centerPane.widthProperty().multiply(0.5).subtract(centerSquarePane.widthProperty().multiply(0.5)));
                centerSquarePane.layoutYProperty().bind(centerPane.heightProperty().multiply(0.5).subtract(centerSquarePane.heightProperty().multiply(0.5)));
                Rectangle bounds = new Rectangle();
                bounds.heightProperty().bind(centerSquarePane.heightProperty());
                bounds.widthProperty().bind(centerSquarePane.widthProperty());
                centerSquarePane.setClip(bounds);
            }
            leftPane = new Pane();
            mainPane.getChildren().add(leftPane);
            leftPane.prefWidthProperty().bind(mainPane.widthProperty().subtract(centerPane.widthProperty()).multiply(0.5));
            leftPane.prefHeightProperty().bind(mainPane.heightProperty());
            {
                double leftBuffersUsed = 15d;
                double leftHeightsUsed = 14d;
                double leftBuffersPerHeight = 8d;
                double leftTotalUnitCount = leftBuffersUsed + (leftHeightsUsed * leftBuffersPerHeight);
                double leftHeightScale = leftBuffersPerHeight / leftTotalUnitCount;
                double leftBufferScale = 1 / leftTotalUnitCount;
                //filePane is 9/10 of the width of the left pane, and 3 heights and 2 buffers tall
                filePane = new Pane();
                leftPane.getChildren().add(filePane);
                filePane.prefWidthProperty().bind(leftPane.widthProperty().multiply(0.9));
                filePane.prefHeightProperty().bind(leftPane.heightProperty().multiply(3 * leftHeightScale + 2 * leftBufferScale));
                filePane.layoutXProperty().bind(leftPane.widthProperty().multiply(0.05));
                filePane.layoutYProperty().bind(leftPane.heightProperty().multiply(leftBufferScale));
                {
                    fileTitle = new Label("Save File:");
                    filePane.getChildren().add(fileTitle);
                    fileTitle.prefWidthProperty().bind(filePane.widthProperty());
                    fileTitle.prefHeightProperty().bind(leftPane.heightProperty().multiply(leftHeightScale));
                    fileTitle.layoutXProperty().setValue(0);
                    fileTitle.layoutYProperty().setValue(0);
                    fileTitle.setAlignment(Pos.CENTER);
                    fileLocationBox = new TextField("./DecayDisplay.save");
                    filePane.getChildren().add(fileLocationBox);
                    fileLocationBox.prefWidthProperty().bind(filePane.widthProperty().multiply(0.9));
                    fileLocationBox.prefHeightProperty().bind(leftPane.heightProperty().multiply(leftHeightScale));
                    fileLocationBox.layoutXProperty().bind(filePane.widthProperty().multiply(0.05));
                    fileLocationBox.layoutYProperty().bind(leftPane.heightProperty().multiply(leftHeightScale));
                    fileLocationBox.setAlignment(Pos.CENTER);
                    loadBtn = new Button("Load");
                    filePane.getChildren().add(loadBtn);
                    loadBtn.prefWidthProperty().bind(filePane.widthProperty().multiply(0.41));
                    loadBtn.prefHeightProperty().bind(leftPane.heightProperty().multiply(leftHeightScale));
                    loadBtn.layoutXProperty().bind(filePane.widthProperty().multiply(0.06));
                    loadBtn.layoutYProperty().bind(leftPane.heightProperty().multiply(2 * leftHeightScale + leftBufferScale));
                    loadBtn.setAlignment(Pos.CENTER);
                    saveBtn = new Button("Save");
                    filePane.getChildren().add(saveBtn);
                    saveBtn.prefWidthProperty().bind(filePane.widthProperty().multiply(0.41));
                    saveBtn.prefHeightProperty().bind(leftPane.heightProperty().multiply(leftHeightScale));
                    saveBtn.layoutXProperty().bind(filePane.widthProperty().multiply(0.53));
                    saveBtn.layoutYProperty().bind(leftPane.heightProperty().multiply(2 * leftHeightScale + leftBufferScale));
                    saveBtn.setAlignment(Pos.CENTER);
                }
                manualDefinitionPane = new Pane();
                leftPane.getChildren().add(manualDefinitionPane);
                manualDefinitionPane.prefWidthProperty().bind(leftPane.widthProperty().multiply(0.9));
                manualDefinitionPane.prefHeightProperty().bind(leftPane.heightProperty().multiply(8 * leftHeightScale + 7 * leftBufferScale));
                manualDefinitionPane.layoutXProperty().bind(leftPane.widthProperty().multiply(0.05));
                manualDefinitionPane.layoutYProperty().bind(leftPane.heightProperty().multiply(leftBufferScale).add(filePane.heightProperty()).add(filePane.layoutYProperty()));
                {
                    manualTitle = new Label("Manual Counts:");
                    manualDefinitionPane.getChildren().add(manualTitle);
                    manualTitle.prefWidthProperty().bind(manualDefinitionPane.widthProperty());
                    manualTitle.prefHeightProperty().bind(leftPane.heightProperty().multiply(leftHeightScale));
                    manualTitle.layoutXProperty().set(0);
                    manualTitle.layoutYProperty().set(0);
                    manualTitle.setAlignment(Pos.CENTER);
                    protonDefinitionPane = new Pane();
                    manualDefinitionPane.getChildren().add(protonDefinitionPane);
                    protonDefinitionPane.prefWidthProperty().bind(manualDefinitionPane.widthProperty().multiply(0.9));
                    protonDefinitionPane.prefHeightProperty().bind(leftPane.heightProperty().multiply(2 * leftHeightScale + leftBufferScale));
                    protonDefinitionPane.layoutXProperty().bind(manualDefinitionPane.widthProperty().multiply(0.05));
                    protonDefinitionPane.layoutYProperty().bind(leftPane.heightProperty().multiply(leftHeightScale));
                    {
                        protonTitle = new Label("Protons:");
                        protonDefinitionPane.getChildren().add(protonTitle);
                        protonTitle.prefWidthProperty().bind(protonDefinitionPane.widthProperty());
                        protonTitle.prefHeightProperty().bind(leftPane.heightProperty().multiply(leftHeightScale));
                        protonTitle.layoutXProperty().set(0);
                        protonTitle.layoutYProperty().set(0);
                        protonTitle.setAlignment(Pos.CENTER);
                        protonBox = new TextField("1");
                        protonDefinitionPane.getChildren().add(protonBox);
                        protonBox.prefWidthProperty().bind(protonDefinitionPane.widthProperty().multiply(0.9));
                        protonBox.prefHeightProperty().bind(leftPane.heightProperty().multiply(leftHeightScale));
                        protonBox.layoutXProperty().bind(protonDefinitionPane.widthProperty().multiply(0.05));
                        protonBox.layoutYProperty().bind(leftPane.heightProperty().multiply(leftHeightScale));
                        protonBox.setAlignment(Pos.CENTER);
                    }
                    neutronDefinitionPane = new Pane();
                    manualDefinitionPane.getChildren().add(neutronDefinitionPane);
                    neutronDefinitionPane.prefWidthProperty().bind(manualDefinitionPane.widthProperty().multiply(0.9));
                    neutronDefinitionPane.prefHeightProperty().bind(leftPane.heightProperty().multiply(2 * leftHeightScale + leftBufferScale));
                    neutronDefinitionPane.layoutXProperty().bind(manualDefinitionPane.widthProperty().multiply(0.05));
                    neutronDefinitionPane.layoutYProperty().bind(protonDefinitionPane.layoutYProperty().add(protonDefinitionPane.heightProperty()).add(leftPane.heightProperty().multiply(leftBufferScale)));
                    {
                        neutronTitle = new Label("Neutrons:");
                        neutronDefinitionPane.getChildren().add(neutronTitle);
                        neutronTitle.prefWidthProperty().bind(neutronDefinitionPane.widthProperty());
                        neutronTitle.prefHeightProperty().bind(leftPane.heightProperty().multiply(leftHeightScale));
                        neutronTitle.layoutXProperty().set(0);
                        neutronTitle.layoutYProperty().set(0);
                        neutronTitle.setAlignment(Pos.CENTER);
                        neutronBox = new TextField("1");
                        neutronDefinitionPane.getChildren().add(neutronBox);
                        neutronBox.prefWidthProperty().bind(neutronDefinitionPane.widthProperty().multiply(0.9));
                        neutronBox.prefHeightProperty().bind(leftPane.heightProperty().multiply(leftHeightScale));
                        neutronBox.layoutXProperty().bind(neutronDefinitionPane.widthProperty().multiply(0.05));
                        neutronBox.layoutYProperty().bind(leftPane.heightProperty().multiply(leftHeightScale));
                        neutronBox.setAlignment(Pos.CENTER);
                    }
                    electronDefinitionPane = new Pane();
                    manualDefinitionPane.getChildren().add(electronDefinitionPane);
                    electronDefinitionPane.prefWidthProperty().bind(manualDefinitionPane.widthProperty().multiply(0.9));
                    electronDefinitionPane.prefHeightProperty().bind(leftPane.heightProperty().multiply(2 * leftHeightScale + leftBufferScale));
                    electronDefinitionPane.layoutXProperty().bind(manualDefinitionPane.widthProperty().multiply(0.05));
                    electronDefinitionPane.layoutYProperty().bind(neutronDefinitionPane.layoutYProperty().add(neutronDefinitionPane.heightProperty()).add(leftPane.heightProperty().multiply(leftBufferScale)));
                    {
                        electronTitle = new Label("Electrons:");
                        electronDefinitionPane.getChildren().add(electronTitle);
                        electronTitle.prefWidthProperty().bind(electronDefinitionPane.widthProperty());
                        electronTitle.prefHeightProperty().bind(leftPane.heightProperty().multiply(leftHeightScale));
                        electronTitle.layoutXProperty().set(0);
                        electronTitle.layoutYProperty().set(0);
                        electronTitle.setAlignment(Pos.CENTER);
                        electronBox = new TextField("1");
                        electronDefinitionPane.getChildren().add(electronBox);
                        electronBox.prefWidthProperty().bind(electronDefinitionPane.widthProperty().multiply(0.9));
                        electronBox.prefHeightProperty().bind(leftPane.heightProperty().multiply(leftHeightScale));
                        electronBox.layoutXProperty().bind(electronDefinitionPane.widthProperty().multiply(0.05));
                        electronBox.layoutYProperty().bind(leftPane.heightProperty().multiply(leftHeightScale));
                        electronBox.setAlignment(Pos.CENTER);
                    }
                    manualDefinitionBtn = new Button("Set Atom");
                    manualDefinitionPane.getChildren().add(manualDefinitionBtn);
                    manualDefinitionBtn.prefWidthProperty().bind(manualDefinitionPane.widthProperty().multiply(0.9));
                    manualDefinitionBtn.prefHeightProperty().bind(leftPane.heightProperty().multiply(leftHeightScale));
                    manualDefinitionBtn.layoutXProperty().bind(manualDefinitionPane.widthProperty().multiply(0.05));
                    manualDefinitionBtn.layoutYProperty().bind(electronDefinitionPane.layoutYProperty().add(electronDefinitionPane.heightProperty()).add(leftPane.heightProperty().multiply(leftBufferScale)));
                    manualDefinitionBtn.setAlignment(Pos.CENTER);
                }
                numberDefinitionPane = new Pane();
                leftPane.getChildren().add(numberDefinitionPane);
                numberDefinitionPane.prefWidthProperty().bind(leftPane.widthProperty().multiply(0.9));
                numberDefinitionPane.prefHeightProperty().bind(leftPane.heightProperty().multiply(3 * leftHeightScale + 2 * leftBufferScale));
                numberDefinitionPane.layoutXProperty().bind(leftPane.widthProperty().multiply(0.05));
                numberDefinitionPane.layoutYProperty().bind(manualDefinitionPane.heightProperty().add(manualDefinitionPane.layoutYProperty()).add(leftPane.heightProperty().multiply(leftBufferScale)));
                {
                    numberTitle = new Label("AtomicNumber:");
                    numberDefinitionPane.getChildren().add(numberTitle);
                    numberTitle.prefWidthProperty().bind(numberDefinitionPane.widthProperty());
                    numberTitle.prefHeightProperty().bind(leftPane.heightProperty().multiply(leftHeightScale));
                    numberTitle.layoutXProperty().set(0);
                    numberTitle.layoutYProperty().set(0);
                    numberTitle.setAlignment(Pos.CENTER);
                    numberBox = new TextField("1");
                    numberDefinitionPane.getChildren().add(numberBox);
                    numberBox.prefWidthProperty().bind(numberDefinitionPane.widthProperty().multiply(0.9));
                    numberBox.prefHeightProperty().bind(leftPane.heightProperty().multiply(leftHeightScale));
                    numberBox.layoutXProperty().bind(numberDefinitionPane.widthProperty().multiply(0.05));
                    numberBox.layoutYProperty().bind(leftPane.heightProperty().multiply(leftHeightScale));
                    numberBox.setAlignment(Pos.CENTER);
                    numberDefinitionBtn = new Button("Set Atom");
                    numberDefinitionPane.getChildren().add(numberDefinitionBtn);
                    numberDefinitionBtn.prefWidthProperty().bind(numberDefinitionPane.widthProperty().multiply(0.9));
                    numberDefinitionBtn.prefHeightProperty().bind(leftPane.heightProperty().multiply(leftHeightScale));
                    numberDefinitionBtn.layoutXProperty().bind(numberDefinitionPane.widthProperty().multiply(0.05));
                    numberDefinitionBtn.layoutYProperty().bind(leftPane.heightProperty().multiply(2 * leftHeightScale + leftBufferScale));
                    numberDefinitionBtn.setAlignment(Pos.CENTER);
                }
            }
            rightPane = new Pane();
            mainPane.getChildren().add(rightPane);
            rightPane.layoutXProperty().bind(centerPane.layoutXProperty().add(centerPane.widthProperty()));
            rightPane.prefWidthProperty().bind(mainPane.widthProperty().subtract(centerPane.widthProperty()).multiply(0.5));
            rightPane.prefHeightProperty().bind(mainPane.heightProperty());
            {
                double buffersUsed = 17d;
                double heightsUsed = 17d;
                double buffersPerHeight = 8d;
                double totalUnitCount = buffersUsed + (heightsUsed * buffersPerHeight);
                double heightScale = buffersPerHeight / totalUnitCount;
                double bufferScale = 1 / totalUnitCount;
                infoPane = new Pane();
                rightPane.getChildren().add(infoPane);
                infoPane.prefWidthProperty().bind(rightPane.widthProperty().multiply(0.9));
                infoPane.prefHeightProperty().bind(rightPane.heightProperty().multiply(8 * heightScale + 5 * bufferScale));
                infoPane.layoutXProperty().bind(rightPane.widthProperty().multiply(0.05));
                infoPane.layoutYProperty().bind(rightPane.heightProperty().multiply(bufferScale));
                {
                    nameTitle = new Label("Periodic Table Name:");
                    infoPane.getChildren().add(nameTitle);
                    nameTitle.prefWidthProperty().bind(infoPane.widthProperty());
                    nameTitle.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    nameTitle.layoutXProperty().set(0);
                    nameTitle.layoutYProperty().set(0);
                    nameTitle.setAlignment(Pos.CENTER);
                    nameBox = new TextField("Not Applicable");
                    infoPane.getChildren().add(nameBox);
                    nameBox.prefWidthProperty().bind(infoPane.widthProperty().multiply(0.9));
                    nameBox.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    nameBox.layoutXProperty().bind(infoPane.widthProperty().multiply(0.05));
                    nameBox.layoutYProperty().bind(nameTitle.layoutYProperty().add(nameTitle.heightProperty()));
                    nameBox.setAlignment(Pos.CENTER);
                    nameBox.setEditable(false);
                    atomicNumberTitle = new Label("Atomic Number:");
                    infoPane.getChildren().add(atomicNumberTitle);
                    atomicNumberTitle.prefWidthProperty().bind(infoPane.widthProperty());
                    atomicNumberTitle.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    atomicNumberTitle.layoutXProperty().set(0);
                    atomicNumberTitle.layoutYProperty().bind(nameBox.layoutYProperty().add(nameBox.heightProperty()));
                    atomicNumberTitle.setAlignment(Pos.CENTER);
                    atomicNumberBox = new TextField("0");
                    infoPane.getChildren().add(atomicNumberBox);
                    atomicNumberBox.prefWidthProperty().bind(infoPane.widthProperty().multiply(0.9));
                    atomicNumberBox.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    atomicNumberBox.layoutXProperty().bind(infoPane.widthProperty().multiply(0.05));
                    atomicNumberBox.layoutYProperty().bind(atomicNumberTitle.layoutYProperty().add(atomicNumberTitle.heightProperty()));
                    atomicNumberBox.setAlignment(Pos.CENTER);
                    atomicNumberBox.setEditable(false);
                    classTitle = new Label("Class:");
                    infoPane.getChildren().add(classTitle);
                    classTitle.prefWidthProperty().bind(infoPane.widthProperty().multiply(0.4));
                    classTitle.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    classTitle.layoutXProperty().bind(infoPane.widthProperty().multiply(0.05));
                    classTitle.layoutYProperty().bind(atomicNumberBox.layoutYProperty().add(atomicNumberBox.heightProperty()).add(rightPane.heightProperty().multiply(bufferScale)));
                    classBox = new TextField("Unknown");
                    infoPane.getChildren().add(classBox);
                    classBox.prefWidthProperty().bind(infoPane.widthProperty().multiply(0.4));
                    classBox.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    classBox.layoutXProperty().bind(infoPane.widthProperty().multiply(0.55));
                    classBox.layoutYProperty().bind(classTitle.layoutYProperty());
                    classBox.setAlignment(Pos.CENTER);
                    classBox.setEditable(false);
                    neutronEstimateTitle = new Label("N Tendency:");
                    infoPane.getChildren().add(neutronEstimateTitle);
                    neutronEstimateTitle.prefWidthProperty().bind(infoPane.widthProperty().multiply(0.4));
                    neutronEstimateTitle.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    neutronEstimateTitle.layoutXProperty().bind(infoPane.widthProperty().multiply(0.05));
                    neutronEstimateTitle.layoutYProperty().bind(classBox.layoutYProperty().add(classBox.heightProperty()).add(rightPane.heightProperty().multiply(bufferScale)));
                    neutronEstimateBox = new TextField("-1");
                    infoPane.getChildren().add(neutronEstimateBox);
                    neutronEstimateBox.prefWidthProperty().bind(infoPane.widthProperty().multiply(0.4));
                    neutronEstimateBox.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    neutronEstimateBox.layoutXProperty().bind(infoPane.widthProperty().multiply(0.55));
                    neutronEstimateBox.layoutYProperty().bind(neutronEstimateTitle.layoutYProperty());
                    neutronEstimateBox.setAlignment(Pos.CENTER);
                    neutronEstimateBox.setEditable(false);
                    chargeTitle = new Label("Charge:");
                    infoPane.getChildren().add(chargeTitle);
                    chargeTitle.prefWidthProperty().bind(infoPane.widthProperty().multiply(0.4));
                    chargeTitle.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    chargeTitle.layoutXProperty().bind(infoPane.widthProperty().multiply(0.05));
                    chargeTitle.layoutYProperty().bind(neutronEstimateBox.layoutYProperty().add(neutronEstimateBox.heightProperty()).add(rightPane.heightProperty().multiply(bufferScale)));
                    chargeBox = new TextField("-1");
                    infoPane.getChildren().add(chargeBox);
                    chargeBox.prefWidthProperty().bind(infoPane.widthProperty().multiply(0.4));
                    chargeBox.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    chargeBox.layoutXProperty().bind(infoPane.widthProperty().multiply(0.55));
                    chargeBox.layoutYProperty().bind(chargeTitle.layoutYProperty());
                    chargeBox.setAlignment(Pos.CENTER);
                    chargeBox.setEditable(false);
                    weightTitle = new Label("Weight:");
                    infoPane.getChildren().add(weightTitle);
                    weightTitle.prefWidthProperty().bind(infoPane.widthProperty().multiply(0.4));
                    weightTitle.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    weightTitle.layoutXProperty().bind(infoPane.widthProperty().multiply(0.05));
                    weightTitle.layoutYProperty().bind(chargeBox.layoutYProperty().add(chargeBox.heightProperty()).add(rightPane.heightProperty().multiply(bufferScale)));
                    weightBox = new TextField("0 AMU");
                    infoPane.getChildren().add(weightBox);
                    weightBox.prefWidthProperty().bind(infoPane.widthProperty().multiply(0.4));
                    weightBox.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    weightBox.layoutXProperty().bind(infoPane.widthProperty().multiply(0.55));
                    weightBox.layoutYProperty().bind(weightTitle.layoutYProperty());
                    weightBox.setAlignment(Pos.CENTER);
                    weightBox.setEditable(false);
                }
                PNEPane = new Pane();
                rightPane.getChildren().add(PNEPane);
                PNEPane.prefWidthProperty().bind(rightPane.widthProperty().multiply(0.9));
                PNEPane.prefHeightProperty().bind(rightPane.heightProperty().multiply(6 * heightScale + 7 * bufferScale));
                PNEPane.layoutXProperty().bind(rightPane.widthProperty().multiply(0.05));
                PNEPane.layoutYProperty().bind(infoPane.layoutYProperty().add(infoPane.heightProperty()).add(rightPane.heightProperty().multiply(bufferScale)));
                {
                    protonCountTitle = new Label("Protons:");
                    PNEPane.getChildren().add(protonCountTitle);
                    protonCountTitle.prefWidthProperty().bind(PNEPane.widthProperty().multiply(0.4));
                    protonCountTitle.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    protonCountTitle.layoutXProperty().bind(PNEPane.widthProperty().multiply(0.05));
                    protonCountTitle.layoutYProperty().bind(rightPane.heightProperty().multiply(bufferScale));//aboveBtn.layoutYProperty()+aboveBtn.heightProperty()+rightPane.heightProperty().multiply(bufferScale));
                    protonCountBox = new TextField("0");
                    PNEPane.getChildren().add(protonCountBox);
                    protonCountBox.prefWidthProperty().bind(PNEPane.widthProperty().multiply(0.4));
                    protonCountBox.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    protonCountBox.layoutXProperty().bind(PNEPane.widthProperty().multiply(0.55));
                    protonCountBox.layoutYProperty().bind(protonCountTitle.layoutYProperty());
                    protonCountBox.setAlignment(Pos.CENTER);
                    protonCountBox.setEditable(false);
                    addProtonBtn = new Button("+P");
                    PNEPane.getChildren().add(addProtonBtn);
                    addProtonBtn.prefWidthProperty().bind(PNEPane.widthProperty().multiply(0.4));
                    addProtonBtn.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    addProtonBtn.layoutXProperty().bind(PNEPane.widthProperty().multiply(0.08));
                    addProtonBtn.layoutYProperty().bind(protonCountBox.layoutYProperty().add(protonCountBox.heightProperty()).add(rightPane.heightProperty().multiply(bufferScale)));
                    addProtonBtn.setAlignment(Pos.CENTER);
                    remProtonBtn = new Button("-P");
                    PNEPane.getChildren().add(remProtonBtn);
                    remProtonBtn.prefWidthProperty().bind(PNEPane.widthProperty().multiply(0.4));
                    remProtonBtn.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    remProtonBtn.layoutXProperty().bind(PNEPane.widthProperty().multiply(0.52));
                    remProtonBtn.layoutYProperty().bind(protonCountBox.layoutYProperty().add(protonCountBox.heightProperty()).add(rightPane.heightProperty().multiply(bufferScale)));
                    remProtonBtn.setAlignment(Pos.CENTER);

                    neutronCountTitle = new Label("Neutrons:");
                    PNEPane.getChildren().add(neutronCountTitle);
                    neutronCountTitle.prefWidthProperty().bind(PNEPane.widthProperty().multiply(0.4));
                    neutronCountTitle.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    neutronCountTitle.layoutXProperty().bind(PNEPane.widthProperty().multiply(0.05));
                    neutronCountTitle.layoutYProperty().bind(remProtonBtn.layoutYProperty().add(remProtonBtn.heightProperty()).add(rightPane.heightProperty().multiply(bufferScale)));
                    neutronCountBox = new TextField("0");
                    PNEPane.getChildren().add(neutronCountBox);
                    neutronCountBox.prefWidthProperty().bind(PNEPane.widthProperty().multiply(0.4));
                    neutronCountBox.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    neutronCountBox.layoutXProperty().bind(PNEPane.widthProperty().multiply(0.55));
                    neutronCountBox.layoutYProperty().bind(neutronCountTitle.layoutYProperty());
                    neutronCountBox.setAlignment(Pos.CENTER);
                    neutronCountBox.setEditable(false);
                    addNeutronBtn = new Button("+N");
                    PNEPane.getChildren().add(addNeutronBtn);
                    addNeutronBtn.prefWidthProperty().bind(PNEPane.widthProperty().multiply(0.4));
                    addNeutronBtn.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    addNeutronBtn.layoutXProperty().bind(PNEPane.widthProperty().multiply(0.08));
                    addNeutronBtn.layoutYProperty().bind(neutronCountBox.layoutYProperty().add(neutronCountBox.heightProperty()).add(rightPane.heightProperty().multiply(bufferScale)));
                    addNeutronBtn.setAlignment(Pos.CENTER);
                    remNeutronBtn = new Button("-N");
                    PNEPane.getChildren().add(remNeutronBtn);
                    remNeutronBtn.prefWidthProperty().bind(PNEPane.widthProperty().multiply(0.4));
                    remNeutronBtn.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    remNeutronBtn.layoutXProperty().bind(PNEPane.widthProperty().multiply(0.52));
                    remNeutronBtn.layoutYProperty().bind(neutronCountBox.layoutYProperty().add(neutronCountBox.heightProperty()).add(rightPane.heightProperty().multiply(bufferScale)));
                    remNeutronBtn.setAlignment(Pos.CENTER);

                    electronCountTitle = new Label("Electrons:");
                    PNEPane.getChildren().add(electronCountTitle);
                    electronCountTitle.prefWidthProperty().bind(PNEPane.widthProperty().multiply(0.4));
                    electronCountTitle.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    electronCountTitle.layoutXProperty().bind(PNEPane.widthProperty().multiply(0.05));
                    electronCountTitle.layoutYProperty().bind(remNeutronBtn.layoutYProperty().add(remNeutronBtn.heightProperty()).add(rightPane.heightProperty().multiply(bufferScale)));
                    electronCountBox = new TextField("0");
                    PNEPane.getChildren().add(electronCountBox);
                    electronCountBox.prefWidthProperty().bind(PNEPane.widthProperty().multiply(0.4));
                    electronCountBox.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    electronCountBox.layoutXProperty().bind(PNEPane.widthProperty().multiply(0.55));
                    electronCountBox.layoutYProperty().bind(electronCountTitle.layoutYProperty());
                    electronCountBox.setAlignment(Pos.CENTER);
                    electronCountBox.setEditable(false);
                    addElectronBtn = new Button("+e");
                    PNEPane.getChildren().add(addElectronBtn);
                    addElectronBtn.prefWidthProperty().bind(PNEPane.widthProperty().multiply(0.4));
                    addElectronBtn.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    addElectronBtn.layoutXProperty().bind(PNEPane.widthProperty().multiply(0.08));
                    addElectronBtn.layoutYProperty().bind(electronCountBox.layoutYProperty().add(electronCountBox.heightProperty()).add(rightPane.heightProperty().multiply(bufferScale)));
                    addElectronBtn.setAlignment(Pos.CENTER);
                    remElectronBtn = new Button("-e");
                    PNEPane.getChildren().add(remElectronBtn);
                    remElectronBtn.prefWidthProperty().bind(PNEPane.widthProperty().multiply(0.4));
                    remElectronBtn.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    remElectronBtn.layoutXProperty().bind(PNEPane.widthProperty().multiply(0.52));
                    remElectronBtn.layoutYProperty().bind(electronCountBox.layoutYProperty().add(electronCountBox.heightProperty()).add(rightPane.heightProperty().multiply(bufferScale)));
                    remElectronBtn.setAlignment(Pos.CENTER);
                }
                radiationPane = new Pane();
                rightPane.getChildren().add(radiationPane);
                radiationPane.prefWidthProperty().bind(rightPane.widthProperty().multiply(0.9));
                radiationPane.prefHeightProperty().bind(rightPane.heightProperty().multiply(3 * heightScale + bufferScale));
                radiationPane.layoutXProperty().bind(rightPane.widthProperty().multiply(0.05));
                radiationPane.layoutYProperty().bind(PNEPane.layoutYProperty().add(PNEPane.heightProperty()).add(rightPane.heightProperty().multiply(bufferScale)));
                {
                    radiationTitle = new Label("Emit/Un-emit Radiation:");
                    radiationPane.getChildren().add(radiationTitle);
                    radiationTitle.prefWidthProperty().bind(radiationPane.widthProperty());
                    radiationTitle.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    radiationTitle.layoutXProperty().set(0);
                    radiationTitle.layoutYProperty().set(0);
                    radiationTitle.setAlignment(Pos.CENTER);

                    alphaEmitBtn = new Button("~α");
                    radiationPane.getChildren().add(alphaEmitBtn);
                    alphaEmitBtn.prefWidthProperty().bind(radiationPane.widthProperty().multiply((1d - 0.12) / 3d));
                    alphaEmitBtn.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    alphaEmitBtn.layoutXProperty().bind(radiationPane.widthProperty().multiply(0.04));
                    alphaEmitBtn.layoutYProperty().bind(radiationTitle.layoutYProperty().add(radiationTitle.heightProperty()));
                    alphaEmitBtn.setAlignment(Pos.CENTER);
                    alphaAbsBtn = new Button("α↩");
                    radiationPane.getChildren().add(alphaAbsBtn);
                    alphaAbsBtn.prefWidthProperty().bind(radiationPane.widthProperty().multiply((1d - 0.12) / 3d));
                    alphaAbsBtn.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    alphaAbsBtn.layoutXProperty().bind(radiationPane.widthProperty().multiply(0.04));
                    alphaAbsBtn.layoutYProperty().bind(alphaEmitBtn.layoutYProperty().add(alphaEmitBtn.heightProperty()));
                    alphaAbsBtn.setAlignment(Pos.CENTER);

                    betaEmitBtn = new Button("~β");
                    radiationPane.getChildren().add(betaEmitBtn);
                    betaEmitBtn.prefWidthProperty().bind(radiationPane.widthProperty().multiply((1d - 0.12) / 3d));
                    betaEmitBtn.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    betaEmitBtn.layoutXProperty().bind(radiationPane.widthProperty().multiply((1d - 0.12) / 3d + 0.06));
                    betaEmitBtn.layoutYProperty().bind(radiationTitle.layoutYProperty().add(radiationTitle.heightProperty()));
                    betaEmitBtn.setAlignment(Pos.CENTER);
                    betaAbsBtn = new Button("β↩");
                    radiationPane.getChildren().add(betaAbsBtn);
                    betaAbsBtn.prefWidthProperty().bind(radiationPane.widthProperty().multiply((1d - 0.12) / 3d));
                    betaAbsBtn.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    betaAbsBtn.layoutXProperty().bind(radiationPane.widthProperty().multiply((1d - 0.12) / 3d + 0.06));
                    betaAbsBtn.layoutYProperty().bind(betaEmitBtn.layoutYProperty().add(betaEmitBtn.heightProperty()));
                    betaAbsBtn.setAlignment(Pos.CENTER);

                    gammaEmitBtn = new Button("~γ");
                    radiationPane.getChildren().add(gammaEmitBtn);
                    gammaEmitBtn.prefWidthProperty().bind(radiationPane.widthProperty().multiply((1d - 0.12) / 3d));
                    gammaEmitBtn.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    gammaEmitBtn.layoutXProperty().bind(radiationPane.widthProperty().multiply((1d - 0.12) * 2 / 3d + 0.08));
                    gammaEmitBtn.layoutYProperty().bind(radiationTitle.layoutYProperty().add(radiationTitle.heightProperty()));
                    gammaEmitBtn.setAlignment(Pos.CENTER);
                    gammaAbsBtn = new Button("γ↩");
                    radiationPane.getChildren().add(gammaAbsBtn);
                    gammaAbsBtn.prefWidthProperty().bind(radiationPane.widthProperty().multiply((1d - 0.12) / 3d));
                    gammaAbsBtn.prefHeightProperty().bind(rightPane.heightProperty().multiply(heightScale));
                    gammaAbsBtn.layoutXProperty().bind(radiationPane.widthProperty().multiply((1d - 0.12) * 2 / 3d + 0.08));
                    gammaAbsBtn.layoutYProperty().bind(gammaEmitBtn.layoutYProperty().add(gammaEmitBtn.heightProperty()));
                    gammaAbsBtn.setAlignment(Pos.CENTER);
                }
            }
        }

    }
    //Mark - these are used to animate the flying particles
    public static void animateParticle(RadiationParticle iAmRadiation, boolean emitting, int type) {
        //System.out.println("run");
        TranslateTransition emittedParticle = new TranslateTransition(Duration.minutes(0.1+Math.random()/6d), iAmRadiation.particlePane);
        animations.add(emittedParticle);
        double direction = Math.random()*2*Math.PI;
        double xDistance = Math.sin(direction);
        double yDistance = Math.cos(direction);
        double distanceToEdge = centerSquarePane.getPrefHeight()/2;
        //System.out.println("["+xDistance+","+yDistance+"]");
        if (Math.abs(xDistance) > Math.abs(yDistance)) {
            //System.out.println("a");
            //noinspection SuspiciousNameCombination
            yDistance /= xDistance;
            xDistance /=Math.abs(xDistance);
        } else {
            //noinspection SuspiciousNameCombination
            xDistance /= yDistance;
            yDistance /=Math.abs(yDistance);
        }
        //System.out.println("["+xDistance+","+yDistance+"]");
        xDistance *=distanceToEdge;
        yDistance *=distanceToEdge;
        //System.out.println("["+xDistance+","+yDistance+"] :"+distanceToEdge);
        if (emitting) {
            emittedParticle.setToX(xDistance+distanceToEdge);
            emittedParticle.setToY(yDistance+distanceToEdge);
            emittedParticle.setFromX(distanceToEdge);
            emittedParticle.setFromY(distanceToEdge);
        } else {
            emittedParticle.setToX(distanceToEdge);
            emittedParticle.setToY(distanceToEdge);
            emittedParticle.setFromX(xDistance+distanceToEdge);
            emittedParticle.setFromY(yDistance+distanceToEdge);
        }
        if (iAmRadiation.type == 6) {
            iAmRadiation.particlePane.getTransforms().add(Transform.rotate(Math.toDegrees(direction),0,0));
        }

        iAmRadiation.particlePane.toFront();
        emittedParticle.setInterpolator(Interpolator.LINEAR);
        emittedParticle.setOnFinished(actionEvent -> {
            //System.out.println("done");
            if (!emitting) {
                FX.updateDisplayedAtom();
            }
            removeElement(emittedParticle.getNode());
        });
        if (emitting) {
            FX.updateDisplayedAtom();
        }
        MainAtom.radiation(type, emitting);
        FX.updateInfo();
        addElement(emittedParticle.getNode());
        iAmRadiation.particlePane.toFront();
        emittedParticle.playFromStart();

    }

    //Mark - these are used to manipulate the display after initialization
    public static void updateAll() {
        //setMainAtomParticle(Main.mainAtom);
        updateDisplayedAtom();
        updateInfo();
    }
    public static void updateInfo(){
        System.out.println("infoUpdate");
        nameBox.setText(PeriodicTable.getName(MainAtom.getAtomicNumber()));
        atomicNumberBox.setText(String.valueOf(MainAtom.getAtomicNumber()));
        neutronEstimateBox.setText(PeriodicTable.mostLikelyNeutronCount(MainAtom.getAtomicNumber())+"");
        classBox.setText(PeriodicTable.classOf(MainAtom.getAtomicNumber()));
        chargeBox.setText(MainAtom.getCharge()+"");
        weightBox.setText(MainAtom.getAtomicWeight()+" AMU");
        protonCountBox.setText(String.valueOf(MainAtom.getProtons()));
        neutronCountBox.setText(String.valueOf(MainAtom.getNeutrons()));
        electronCountBox.setText(String.valueOf(MainAtom.getElectrons()));
    }
    public static void updateDisplayedAtom() {
        System.out.println("atomUpdate");
        removeElement(DisplayedAtom.mainPane);
        DisplayedAtom.copyMainAtom();
        addElement(DisplayedAtom.generateBohrModel());
        DisplayedAtom.mainPane.prefWidthProperty().bind(centerSquarePane.widthProperty());
        DisplayedAtom.mainPane.prefHeightProperty().bind(centerSquarePane.heightProperty());
        DisplayedAtom.mainPane.layoutXProperty().bind(centerSquarePane.widthProperty().multiply(0));
        DisplayedAtom.mainPane.layoutYProperty().bind(centerSquarePane.heightProperty().multiply(0));
    }
    @SuppressWarnings("unused") // may not be used in the code, but it's good to have for testing
    public static void clearNonDisplayedAtomElements() {
        centerSquarePane.getChildren().removeIf(node -> !node.equals(DisplayedAtom.mainPane));
    }
    public static void removeElement(Node element) {
        centerSquarePane.getChildren().remove(element);
    }
    public static void addElement(Node element) {
        centerSquarePane.getChildren().add(element);
    }
    //Mark - these are the event handlers for when buttons are pressed
    public static void setupEventHandlers() {
        loadBtn.setOnMousePressed(mouseEvent -> {
            Save.load(fileLocationBox.getText());
            updateAll();
        });
        saveBtn.setOnMousePressed(mouseEvent -> Save.save(fileLocationBox.getText()));
        manualDefinitionBtn.setOnMousePressed(mouseEvent -> {
            try {
                MainAtom.setMainAtom(Integer.parseInt(protonBox.getText()),Integer.parseInt(neutronBox.getText()),Integer.parseInt(electronBox.getText()));
            } catch (NumberFormatException e) {
                Error.notAnIntError();
            }
            updateAll();
        });
        numberDefinitionBtn.setOnMousePressed(mouseEvent -> {
            try {
                MainAtom.setMainAtom(Integer.parseInt(numberBox.getText()));
            } catch (NumberFormatException e) {
                Error.notAnIntError();
            }
            updateAll();
        });
        addProtonBtn.setOnMousePressed(mouseEvent -> new RadiationParticle("p",false));
        remProtonBtn.setOnMousePressed(mouseEvent -> new RadiationParticle("p",true));
        addNeutronBtn.setOnMousePressed(mouseEvent -> new RadiationParticle("n",false));
        remNeutronBtn.setOnMousePressed(mouseEvent -> new RadiationParticle("n",true));
        addElectronBtn.setOnMousePressed(mouseEvent -> new RadiationParticle("e",false));
        remElectronBtn.setOnMousePressed(mouseEvent -> new RadiationParticle("e",true));
        alphaEmitBtn.setOnMousePressed(mouseEvent -> new RadiationParticle("a",true));
        alphaAbsBtn.setOnMousePressed(mouseEvent -> new RadiationParticle("a",false));
        betaEmitBtn.setOnMousePressed(mouseEvent -> new RadiationParticle("b",true));
        betaAbsBtn.setOnMousePressed(mouseEvent -> new RadiationParticle("b",false));
        gammaEmitBtn.setOnMousePressed(mouseEvent -> new RadiationParticle("g",true));
        gammaAbsBtn.setOnMousePressed(mouseEvent -> new RadiationParticle("g",false));

    }
}