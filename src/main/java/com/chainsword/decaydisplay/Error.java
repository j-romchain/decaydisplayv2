package com.chainsword.decaydisplay;

import javafx.scene.control.Alert;

// **********************************************************************************
// Title: DecayDisplay
// Author: Jarom Wright
// Course Section: CMIS201-ONL3 (Seidel) Spring 2023
// File: GUIError.java
// Description: a class for reporting errors that is used to decide weather to report to GUI or not automatically.
// **********************************************************************************
public class Error {
    public static void error(String errorMessage) {
        if (Main.GUIInit) {
            GUIPopupError(errorMessage);
        } else {
            consolePrintlnError(errorMessage);
        }
    }
    public static void GUIPopupError(String errorMessage) {
        Alert error = new Alert(Alert.AlertType.ERROR, errorMessage);
        error.showAndWait();
    }
    public static void consolePrintlnError(String errorMessage) {
        System.out.println("ERROR: "+errorMessage);
    }
    public static void bothError(String errorMessage) {
        consolePrintlnError(errorMessage);
        GUIPopupError(errorMessage);
    }
    public static void argumentCompleteError() {
        bothError("FATAL ERROR: unexpected argument pattern over command line.");
    }
    public static void argumentPartialError() {
        bothError("ERROR: bad arguments were given... this has been handled by ignoring the argument and substituting a default.");
    }
    public static void negativeParticleError() {
        error("ERROR: something is trying to make a negative amount of protons or electrons or neutrons... refusing to go below 0.");
    }
    public static void notAnIntError() {
        error("ERROR: one of the fields that was supposed to be a number, wasn't.");
    }
    public static void loadError() {
        error("ERROR: Error while trying to open Save file... creating file with defaults and loading it...");
    }
    public static void saveError() {
        error("ERROR: Something went wrong with saving... we don't know what.");
    }
}
