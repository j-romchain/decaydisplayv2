package com.chainsword.decaydisplay;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

// **********************************************************************************
// Title: DecayDisplay
// Author: Jarom Wright
// Course Section: CMIS201-ONL3 (Seidel) Spring 2023
// File: NucleusGeneration.java
// Description: this will help me to pack the circles for the nucleus of the MainAtom.
// **********************************************************************************
public class NucleusGeneration {
    static final DoubleProperty hadronRadius = new SimpleDoubleProperty(10);
    private static final double packingStageSize = 5.0;
    public static void assembleNucleus(int neutrons, int protons, Pane neucleusPane) {
        Circle[] parts = customArrange(neutrons+protons);
        tidyUp(parts);
        addCirclesAndBindToPane(parts,neucleusPane);
        assignNeutronProton(parts,neutrons,protons);
    }
    public static Circle[] customArrange(int packerCount) {
        Circle[] packers = new Circle[packerCount];
        if (packerCount >= 1) {
            int layer = 0;
            double distance = 0;
            double layerCircumference;
            double angleBetween = 0;
            int packersLeftForThisLayer = 1;
            for (int i = 0; i < packers.length; i++) {
                Circle packer = new Circle(packingStageSize);
                packers[i] = packer;
                double angle = Math.toRadians(angleBetween * packersLeftForThisLayer);
                packersLeftForThisLayer--;
                //System.out.println(layer);
                //System.out.println(distance);
                double x = distance * Math.sin(angle);
                double y = distance * Math.cos(angle);
                //System.out.println(packersLeftForThisLayer);
                if (packersLeftForThisLayer <= 0) {
                    //System.out.println("hi");
                    layer++;
                    distance = 2 * packingStageSize * layer;
                    layerCircumference = 2 * Math.PI * (distance);
                    packersLeftForThisLayer = (int) Math.floor(layerCircumference / (packingStageSize * 2));
                    angleBetween = (double) 360 / packersLeftForThisLayer;
                }
                packer.setCenterX(x);
                packer.setCenterY(y);
            }
        }
        return packers;
    }
    public static void tidyUp(Circle[] hadrons) {
        double changeAmount = 0.1;
        for (Circle hadron:hadrons) {
            if (hadron!=hadrons[0]) {
                //System.out.println(hadron.getCenterX()+","+hadron.getCenterY());
                int limit = (int) (packingStageSize * 3);
                while ((!circlesOverlap(hadrons, hadron)) && (limit >= 0)) {
                    int centerXNegative = (int) (hadron.getCenterX() / Math.abs(hadron.getCenterX()));
                    int centerYNegative = (int) (hadron.getCenterY() / Math.abs(hadron.getCenterY()));
                    limit--;
                    double newX = Math.abs(hadron.getCenterX());
                    double newY = Math.abs(hadron.getCenterY());

                    double angle = Math.atan(newX/newY);
                    newX -= changeAmount*Math.sin(angle);
                    newY -= changeAmount*Math.cos(angle);

                    hadron.setCenterX(centerXNegative * newX);
                    hadron.setCenterY(centerYNegative * newY);
                }
                int centerXNegative = (int) (hadron.getCenterX() / Math.abs(hadron.getCenterX()));
                int centerYNegative = (int) (hadron.getCenterY() / Math.abs(hadron.getCenterY()));
                double newX = Math.abs(hadron.getCenterX());
                double newY = Math.abs(hadron.getCenterY());

                double angle = Math.atan(newX/newY);
                newX += changeAmount*Math.sin(angle);
                newY += changeAmount*Math.cos(angle);

                hadron.setCenterX(centerXNegative*newX);
                hadron.setCenterY(centerYNegative*newY);
                //System.out.println(hadron.getCenterX()+":"+hadron.getCenterY());
            }
        }
    }
    public static boolean circlesOverlap(Circle[] circles, Circle main) {
        for (Circle circle : circles) {
            if (circle != main) {
                double distance = Math.sqrt(Math.pow(main.getCenterX() - circle.getCenterX(), 2) + Math.pow(main.getCenterY() - circle.getCenterY(), 2));
                if (distance < main.getRadius() + circle.getRadius()) {
                    return true;
                }
            }
        }
        return false;
    }
    public static void assignNeutronProton(Circle[] hadrons, int neutrons, int protons) {
        int total = hadrons.length;
        for (Circle hadron:hadrons) {
            double randomNumberForIfProton = (Math.random()*total);
            if (randomNumberForIfProton<protons) {
                makeProton(hadron);
                protons-=1;
            } else //noinspection StatementWithEmptyBody
                if (randomNumberForIfProton<neutrons+protons) {
                makeNeutron(hadron);
                neutrons-=1;
            } else {
                //leave it generic
            }
            total-=1;
        }
    }
    public static void addCirclesAndBindToPane(Circle[] circles, Pane squarePaneToBindTo) {
        double maxDistance = furthestPointFromCenter(circles);
        DoubleProperty scaleFactor = new SimpleDoubleProperty();
        scaleFactor.bind(squarePaneToBindTo.heightProperty().divide(2).divide(maxDistance)); //new SimpleDoubleProperty(maxDistance).divide(squarePaneToBindTo.heightProperty()));
        hadronRadius.bind(scaleFactor.multiply(packingStageSize));

        for (Circle circle : circles) {
            double X = circle.getCenterX();
            double Y = circle.getCenterY();
            circle.centerXProperty().bind(squarePaneToBindTo.widthProperty().divide(2).add(scaleFactor.multiply(X)));
            circle.centerYProperty().bind(squarePaneToBindTo.heightProperty().divide(2).add(scaleFactor.multiply(Y)));
            squarePaneToBindTo.getChildren().add(circle);
        }
    }
    public static double furthestPointFromCenter(Circle[] circles) {
        double maxDistance = 0.0;
        for (Circle c : circles) {
            double distanceFromCenter = Math.sqrt(Math.pow(c.getCenterX(), 2) + Math.pow(c.getCenterY(), 2));
            double distanceToEdge = distanceFromCenter + c.getRadius();
            if (distanceToEdge > maxDistance) {
                maxDistance = distanceToEdge;
            }
        }
        return maxDistance;
    }
    public static Circle makeProton(Circle toBecomeProton) {
        toBecomeProton.setStroke(Color.BLACK);
        toBecomeProton.setFill(Color.PINK);
        toBecomeProton.radiusProperty().bind(hadronRadius);
        return toBecomeProton;
    }
    public static Circle makeNeutron(Circle toBecomeNeutron) {
        toBecomeNeutron.setStroke(Color.BLACK);
        toBecomeNeutron.setFill(Color.LIGHTYELLOW);
        toBecomeNeutron.radiusProperty().bind(hadronRadius);
        return toBecomeNeutron;
    }
    public static Circle makeElectron(Circle toBecomeElectron) {
        toBecomeElectron.setStroke(Color.BLACK);
        toBecomeElectron.setFill(Color.LIGHTBLUE);
        toBecomeElectron.radiusProperty().bind(hadronRadius.divide(2));
        return toBecomeElectron;
    }
    /*public static Circle[] scaleCirclesToFit(Circle[] circles, double containerRadius) {
        double maxDistance = furthestPointFromCenter(circles);
        double scaleFactor = containerRadius / maxDistance;

        for (Circle circle : circles) {
            double scaledRadius = circle.getRadius() * scaleFactor;
            double scaledX = circle.getCenterX() * scaleFactor;
            double scaledY = circle.getCenterY() * scaleFactor;
            circle.setRadius(scaledRadius);
            circle.setCenterX(scaledX);
            circle.setCenterY(scaledY);
        }

        return circles;
    }*/

}