package com.chainsword.decaydisplay;

// **********************************************************************************
// Title: DecayDisplay
// Author: Jarom Wright
// Course Section: CMIS201-ONL3 (Seidel) Spring 2023
// File: Help.java
// Description: something.
// **********************************************************************************

public class Help {
    public static void main(String[] args) {
        about();
    }

    public static void about() {
        System.out.println("""
                EBook Bleeper Help
                
                    This is a program designed for helping to bleep out
                swear words and such from text files that are in a
                folder array.
                    The program (will have) has a GUI that it runs to guide the user
                through use of the program, under the method "Main.main", with no arguments.
                    In simple terms that means the program defaults to easy mode when run as
                a jar file without arguments.
                    The program also (will have) has two CLIs that can be used to run it either
                in headless mode from another program, or in interactive mode where it tries to
                make it easy like the GUI. They are under the same method as the GUI, but with
                "CLI" as its only argument for user mode, or with a strict set of arguments
                explained in "API.txt" for headless mode.
                
                    The current status of the project is: INIT
                meaning that almost none of the features are functional, and the skeleton of the project is being built.
                """);
    }
}
