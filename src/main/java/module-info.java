module com.chainsword.decaydisplay {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;


    opens com.chainsword.decaydisplay to javafx.fxml;
    exports com.chainsword.decaydisplay;
}