#!/bin/bash

# Define variables for downloading URLs
CORRETTO_URL="https://corretto.aws/downloads/latest/amazon-corretto-20-x64-linux-jdk.tar.gz"
JAVAFX_SDK_URL="https://download2.gluonhq.com/openjfx/20.0.1/openjfx-20.0.1_linux-x64_bin-sdk.zip"
JAVAFX_JMODS_URL="https://download2.gluonhq.com/openjfx/20.0.1/openjfx-20.0.1_linux-x64_bin-jmods.zip"

# Create directories for Java and JavaFX
mkdir -p java jmods javafx javatmp

# Download and extract Corretto JDK
if [ ! -f "java/version.txt" ] || [ "$(cat java/version.txt)" != "20.0.0" ]; then
    rm -rf java
    mkdir java
    echo "Downloading Corretto JDK..."
    wget -O - "$CORRETTO_URL" | tar --directory=javatmp -xz || exit 1
    mv javatmp/*/* java || exit 1
    rm -rf javatmp/* || exit 1
    echo "20.0.0" > java/version.txt
else
    echo "Using cached Corretto JDK"
fi

# Download and extract JavaFX SDK
if [ ! -f "javafx/version.txt" ] || [ "$(cat javafx/version.txt)" != "20.0.1" ]; then
    rm -rf javafx
    mkdir javafx
    echo "Downloading JavaFX SDK..."
    wget -O javatmp/tmp.zip "$JAVAFX_SDK_URL" || exit 1
    unzip javatmp/tmp.zip -d javatmp || exit 1
    rm javatmp/tmp.zip || exit 1
    mv javatmp/*/* javafx || exit 1
    rm -rf javatmp/* || exit 1
    echo "20.0.1" > javafx/version.txt
else
    echo "Using cached JavaFX SDK"
fi

# Download and extract JavaFX JMODs
if [ ! -f "jmods/version.txt" ] || [ "$(cat jmods/version.txt)" != "20.0.1" ]; then
    rm -rf jmods
    mkdir jmods
    echo "Downloading JavaFX JMODs..."
    wget -O javatmp/tmp.zip "$JAVAFX_JMODS_URL" || exit 1
    unzip javatmp/tmp.zip -d javatmp || exit 1
    rm javatmp/tmp.zip || exit 1
    mv javatmp/*/* jmods || exit 1
    rm -rf javatmp/* || exit 1
    echo "20.0.1" > jmods/version.txt
else
    echo "Using cached JavaFX JMODs"
fi

# Remove temporary directory
rm -rf javatmp

echo "Java and JavaFX installation successful"
