![DepricatedNoticePicture](images/depricated.png)
![Icon](images/icon.png)

DecayDisplayV2

This is a small project written as a college assignment. 

THIS IS DEPRECATED, AND NO LONGER MAINTAINED, 
it was replaced by DigitizedDecay found at the following locations:
source:https://gitlab.com/j-romchain/DigitizedDecay
demo:https://j-romchain.gitlab.io/DigitizedDecay
PHET:TBD

this was a copy and streamlining of https://gitlab.com/jromchain/DecayDisplay which was v1, because by having the releases in the git repository it became nigh impossible to clone, and I accidentally gitignored alot of things by forgetting to add a /.

To get started:
  Download and install Java 20 or later, download the release of your choice, and run the file with either "java -jar" from the command line, or by double clicking if your system allows such.

Disclaimer: the accuracy of any information in the app is not guaranteed.

Licence: don't get me in trouble for anything, and don't remove the name DecayDisplay from it, or any url references to the original app. otherwise, you are free to redistribute modify and add whatever you want. as an exception you may copy up to 2 functions worth or 50 lines, whichever is more, and remove references to the original app. this is all under the condition that any legal trouble I may get into as a result of your actions may be deferred upon you and any resulting expense to myself may also be deferred to you.

Screenshots:
![Usage example 1](images/usageExample1.png)
![Usage example 2](images/usageExample2.png)


